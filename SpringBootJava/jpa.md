follow the naming convention for the database like `id_entity`
do not use Camel case

every entity must have its empty constructor in order to work properly

[justin case](https://stackoverflow.com/questions/4011472/mappedby-reference-an-unknown-target-entity-property)

in the config file like this as an example

```java
package com.onpe.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserDetailsService userService;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
			.antMatchers("/", "/signup").permitAll()
			.antMatchers("/css/*", "/imgs/*").permitAll()
			.anyRequest().authenticated()
			.and()
			
			.formLogin().loginPage("/login").loginProcessingUrl("/logincheck")
			.usernameParameter("user").passwordParameter("password")
			.defaultSuccessUrl("/login/loginsuccess").permitAll()
			.and()
			
			.logout().logoutUrl("/logout").logoutSuccessUrl("/login?logout")
			.permitAll();
		
	}
}
```

the part when it says **user parameter** needs to match with the `html` file!!
