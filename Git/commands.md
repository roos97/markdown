## EXAMPLE VARIABLES
- TEXT 
- FILE
- BRANCH
- SHA code of commit
- % the current git command example

## BASICS

- `git status` iforms you about your files can be **A** for added **M** modified, **D** deleted and so on

- `git add` needs an argument and i will add file(s) to the **staging** area
	1. `% .` it will add all your modified or untracked files remeber to be on the root directory of your rep 
	2. `% FILE` it will add an specific file to staging area

- `git commit` commit your changes 
	1. `% -a -m 'TEXT'` firt it'll git add all and the it'll commit 
	2. `% --amend` to rename your last commit it will promt you with vim 

- `git log` show the history of the commands
	1. `% --all` show all the branches 
	2. `% --graph` show a graphical log 	
	3. `% --oneline` won't show that much of info just one line per commit 

- `git branch` creates a new branch
	1. `% TEXT` will create a new branch with the given name 

- `git checkout` go back to a certaint commit to see how it looked like your rep
	1. `% SHA` will take you back to a specific command
	2. `% -b TEXT` will create a new branch with the given name and it will move you there

- `git diff` show you what has changed on your files

- `git show` the same as **git diff** but now you can use it for commits

- `git stash` everything current change goes to the stashing area
	1. `%` will stash all except for the **untracked files**
	2. `% save 'TEXT'` will stash all except for the **untracked files** with custom message
	3. `% -u` will stash all including the untraked files
	4. `% save -u 'TEXT'` will stash all including the untraked files with custom message
	5. `% pop` will take back the latest stash, same as `git stash pop stash@{0}`
	6. `% pop stash@{#}` it will stash a specific stash like 1, 2, 9 etc

## UNDO

- `git reset`
	1. `% --soft` 
	2. `% --mixed` 
	3. `% --hard` 


## WORKING WITH A GIT CLIENT
you can't push until your repo is updated so make sure to `git pull` before pushing

- `git push` it will push the changes from the remote repo default is the current branch
	1. `% origin BRANCH` will push to specific branch

- `git pull` it will pull the changes from the remote repo
	1. `% origin BRANCH` will pull to specific branch

