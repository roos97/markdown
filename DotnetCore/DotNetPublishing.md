# Publishing

## Usign Digital Ocean

1. First you need to create a droplet in digital ocean, then go to
the section **one-click-apps** and select the **LAMP on 16.04**
image, we'll choose this image because it alredy comes with apache and
mysql

2. choose the smalles size which is **$5 per month**

3. choose a datacenter region

4. create a hostname 

## Connecting With SSH

After creating a server you will get an email with the password in it.

In the terminal use the following command

```bash
ssh root@[ipaddress]
```

you will add here the emailed password then the linux machine will tell to change your password so pick a new password and enter it

after that you now can access to your linux server machine

## Creating a Mysql User

Remeber that the user to be created has to match the user from your local machine

There will be a msql password at `/root/.digitalocean_password`, so copy that password
to access into the mql prompt.

### Basic MySql commands

- to acces to the mysql prompt use

```bash
mysql -u root -p
```

- to create a new user

```bash
CREATE USER 'appuser'@'localhost' IDENTIFIED BY 'password';
```
- to grant all access to the created user

```bash
GRANT ALL PRIVILEGES ON * . * TO 'appuser'@'localhost';
```
- to make the priveleges to make effect you need to flush it

```bash
FLUSH PRIVILEGES;
```

- to create a database

```bash
create database mydatabase;
```

- to use that database

```bash
use mydatabase;
```

- to run an external script

```bash
source /root/myscript.sql
```

this will execute the script **don't forget to use the desired database**

- to quit just type quit

## Installing DotNet

just follow this [link](https://www.microsoft.com/net/learn/get-started/linux/ubuntu17-10)

be careful that the version in your server needs to match te exact version in your development computer

be careful choosing the ubuntu version

## Configuring Apache server

First of all we need to configure apache as a reverse server, run this

```bash
a2enmod proxy proxy_http proxy_html
```


Create a file named datingapp.conf

```bash
sudo nano /etc/apache2/conf-enabled/datingapp.conf
```

Inside that file paste the following

```xml
<VirtualHost *:80>
ProxyPreserveHost On
ProxyPass / http://127.0.0.1:5000/
ProxyPassReverse / http://127.0.0.1:5000/
ErrorLog /var/log/apache2/datingapp-error.log
CustomLog /var/log/apache2/datingapp-access.log common
</VirtualHost>
```
Make a restart `sudo service apache2 restart`

## Creating a test app 

In the root directory make a new directory called testapp then inside it `cd testapp` execute `dotnet new mvc`

Once it's created execute `dotnet publish`


## Copy the testapp publish version

Execute the following `sudo cp -a ~/testapp/bin/Debug/netcoreapp2.0/publish/ /var/testapp/`

Now create another config file

```bash
sudo nano /etc/systemd/system/kestrel-testapp.service
```

Inside it paste this

```bash
[Unit]
Description=Example ASP .NET Web Application running on Ubuntu 16.04
[Service]
WorkingDirectory=/var/testapp
ExecStart=/usr/bin/dotnet /var/testapp/testapp.dll
Restart=always
RestartSec=10
SyslogIdentifier=dotnet-demo
User=www-data
Environment=ASPNETCORE_ENVIRONMENT=Production
[Install]
WantedBy=multi-user.target
```
Restar the apache server

```bash
sudo service apache2 restart
```

After that do

```bash
sudo systemctl stop kestrel-testapp.service
sudo systemctl enable kestrel-testapp.service
sudo systemctl start kestrel-testapp.service
```

To make sure if it's running write this command

```bash
netstat -ntpl
```