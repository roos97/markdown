# All About Web APIs

#### *Technology dotnet core*

## Building a Walking Skeleton

---

To create a new web api run the following command:
```bash
 dotnet new webapi
```
notice that we don't specify the name of the project since the project name will be the same as the current directory

There are two types of routing in MVC: convention base and attributed base, the course will be usign the attrubuted base most of the time.
The `app.UseMvc()` only supports the attributed routing, I think that is why we'll be using more attributed routing.

### Changing the environment

The default set environment is Production
To change the environment do the following:

- In Windows Powershell

```powershell
$env:ASPNETCORE_ENVIRONMENT = "Development"
```
- In Mac or Linux Bash

```bash
export ASPNETCORE_ENVIRONMENT=Development
```

### Enableing the watcher

To make a watcher so you don't have to run your project every time you make a change got to the
`[projectName].scproj` (xml) file and the inside `<ItemGroup>` tag add the following:

```xml
  <DotNetCliToolReference Include="Microsoft.DotNet.Watcher.Tools" Version="2.0.0" />
```

then do a `dotnet restore` in terminal and to make it work run
`dotnet watch run`

---

### Start Making Classes

1. Create a folder in the main directory called **Models** inside it create a **class value** which will be then mapped.

2. Create a folder in the main directory call **Data** inside it create a class called **DataContext** this class wil inherit from **DbContext** then set the following config inside the class

```csharp
public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}
        public DbSet<Value> Values { get; set; }
    }
```

3. In the **appsettings.json** file inside **ConnectionStrings** add the following: `"DefaultConnection" : "Data Source=[nameofdatabase].db"` notice *that we are using a sqlite database*

```json
"ConnectionStrings": {
    "DefaultConnection": "Data Source=firstdb.db"
  }
```

4. Finally in the `Startup.cs` class inside the `ConfigureServices` class add the class *AddDbContext*

```csharp
   public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x => x.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));// here pass the reference to the configuracion the appsettings.json file
            services.AddMvc();
        }
```

---

### Configure the Entity Framework

To add entity framework in the cli add in the `[nameofproject]csproj` file inside the `<ItemGroup>` tag add:

```xml
<DotNetCliToolReference Include="Microsoft.EntityFrameworkCore.Tools.DotNet" Version="2.0.0" />
```

Don't forget to restore to enable the EF cli

- To add a migration

```bash
dotnet ef migrations add [nameofthemigration]
```

- To update the database with a new migration

```bash
dotnet ef database update
```

- To generate a script 

```bash
dotnet ef migrations script -o myscript.sql
```
remember this does not create the database, just the tables

---

### Configure class to add the context

To start using the model with the web api controller, in the controller folder do the following

```csharp
  [Route("api/[controller]")]// this is an attributed base routing
  public class ValuesController : Controller
  {
    private readonly DataContext _context;

    public ValuesController(DataContext context)
    {
      _context = context;

    }
  }
```

To start using the `_context` in the controller api methods in this case in the **get** method

```csharp
    [HttpGet]
    public IActionResult GetValues()
    {
      var values = _context.Values.ToList();

      return Ok(values);
    }
```

And for specific values do this

```csharp
    [HttpGet("{id}")]
    public IActionResult GetValue(int id)
    {
      var value = _context.Values.FirstOrDefault(v => v.Id == id);

      return Ok(value);
    }
```

Because synchronous comes with a cost which is performance we're gonna use asychronous as much as posible, so it won't be that difficult because it's easy to implement so always go with asynchronous

We are going to implement in the last to methods the asynchronous approach

```csharp
    [HttpGet]
    public async Task<IActionResult> GetValues()
    {
      var values = await _context.Values.ToListAsync();

      return Ok(values);
    }
```

```csharp
    [HttpGet("{id}")]
    public async Task<IActionResult> GetValue(int id)
    {
      var value = await _context.Values.FirstOrDefaultAsync(v => v.Id == id);

      return Ok(value);
    }
```

---

### Git

When adding the source control git don't forget to exclue the following files into the `.gitignore` file.

```.gitignore
    bin
    obj
    .vscode
    *.db
```

---

### CORS Cross Origin Resource Sharing

If you are making a request from a **different** origin other than the dotnet core api address then you need to enable or configure the cors in your app otherwise your request won't work because of the browser, this is a browser technique to prevent unwittingly loading content from a diferet origin.

So we actully recive a response from the server but the security feature from the brosers prevents us for **manipulating** or **loading** the response content.

One think to point out is that when we make a request from **PostMan** the server sends its response, because technically postman has the same origin address as your web api.

For example if make a request from an angular app which typically is hosted on `localhost:4200` to a dotnet web api which is hosted on `localhost:5000` then the request won't work unless you enable **cors** in the api app.

### Adding CORS to the web api

So to enble cors go to the `Starup.cs` class an add the followings.

Indise the `ConfigureServices` method here the order doesn't matters.

```csharp
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
        }
```

And inside the `Configure` method here the order does matter, *the cors config has to go before the usemvc config*

```csharp
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials()); //these add the header into the request
            app.UseMvc();
        }
```

---

## Security

### Storing passwords in the Database

When it comes to store password in a database when need to use a kind of protection for the user passwords throughout the computer history there has been a lot of ways to secure the passwords. We are gonna be using the password hash password salt method which consist in concadenate the password with a random hash called salt and then encript that.

- Without using the salt our password will look like that, notice that for same paswords its hashes are also the same.

<center>

| Password        | Encription (hash SHA512)|
| ------------- |:-------------:|
| password     | B109F3BBBC244EB824419... |
| password     | B109F3BBBC244EB824419... |
| 1234     | D404559F602EAB6FD602A...    |
| abcd | D8022F2060AD6EFD297AB7...      |

</center>

- Usingh the password with salt hash it creates a more secure encription, and notice that the same passwords don't have the same encriptions

<center>

| Password        | Encription (hash SHA512) + Salt|
| ------------- |:-------------:|
| password     | B109F3BBBC244EB824419... |
| password     | E8347DF324C34323E4343... |
| 1234     | D404559F602EAB6FD602A...    |
| abcd | D8022F2060AD6EFD297AB7...      |

</center>

---

### Creating the User Model

So first we need to create a user class to work with password here a simple user example.


1. Create the user model.

```csharp
public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
    }
```

2. Add it to the data context.

```csharp
    public DbSet<User> Users { get; set; }
```

3. Make a new migration.

```bash
    dotnet ef migrations add [nameofthemigration]
```

4. Update the database.

```bash
    dotnet ef database update
```

---

### The Repository Patter

The repository patter creates another level of abstraction appart for the entity framework abstraction, it separates the implementation when talking to the database form the controller, so the controller doesn't need to know nothing about how to talk to the database *in this case with the entity framework* only need to use the methods like retrieving data or writing data to the database.

So using the repository pattern gives us the following benefits.

- Minimizes duplicate query logic

- Decouples application from persistence framework

- All DB queries in the same place

- Promotes testability

### Creating the Repository Pattern

***you will need to refactor this past later on

So first we create the interface

```csharp
    namespace caja.Repositories
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);
         Task<User> Login(string username, string password);
         Task<bool> UserExists(string username);
    }
}
```


Then we create a class with the same name as the interface but the I, and imlement the interface

```csharp
      public class AuthRepository : IAuthRepository
  {
    private readonly DataContext _context;
    public AuthRepository(DataContext context)
    {
      this._context = context;

    }
    public async Task<User> Login(string username, string password)
    {
      var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);
      // it uses firstOrDefaultAsyncs because in the case that it didn't find the user
      // it will return a null
      // on the other hand the firstAsync will raise and exception if it didn't find the user
      // and (exceptions are expensive so we will user first or default async)

      if (user == null) // the user doesn't exist
        return null;

      // we also need to verify the password

      if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
        return null;

      // notice that we are return null either if the username doesn't exits or if the password doesn't match;	

			// auth successful
			return user;

    }

		// this will work kind of the other way around as the CreatePasswordHash method
    private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
    {
      using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))// notice that he is imorting the class right away from here
      {
        var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
				for (int i = 0; i < computedHash.Length; i++)
				{
						if(computedHash[i] != passwordHash[i]) return false; // as soon as it gest flase then inmediatly we know the dind't match the hashPassword
				}
				return true; // if every element of the array matches then we know that is the right password (authSucceded)
      }
    }

		// first we pass the key (passwordSalt) to the HMACSHA512 method

		// the computedHash password will the as the database password (passwordHash)

		// because our password are arrays of bytes we need to compare them in a loop one by one

    public async Task<User> Register(User user, string password)
    {
      byte[] passwordHash, passwordSalt;
      CreatePasswordHash(password, out passwordHash, out passwordSalt);

      user.PasswordHash = passwordHash;
      user.PasswordSalt = passwordSalt;

      await _context.Users.AddAsync(user);
      await _context.SaveChangesAsync();

      return user;
    }

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
      using (var hmac = new System.Security.Cryptography.HMACSHA512())// notice that he is imorting the class right away from here
      {
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
      }
    }

		// this last one is very simple if there is already a user that username then we return true
    public async Task<bool> UserExists(string username)
    {
      if (await _context.Users.AnyAsync(x => x.Username == username))
				return true;

			return false;
    }
  }
```

## Telling our application that we have a repository

#### we need to make this available for injections in our controllers

So in the `Startup.cs` class insithe the `ConfigureServices` method we specify anithing we wanna use for **dependency injection** for more information *about the three options* go to *lecture 28 at min 0:43*

```csharp
          public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IAuthRepository, AuthRepository>();
        }
```

Notice above the if we need to add another repository we just need to change the AuthRepository *that is the major advantage of using the repository patter*

---

## Implementing the repository in our controllers

Now that we have our repository created we implemented in a new controller class called `AuthController`

```csharp
namespace caja.Controllers
{
  [Route("api/[controller]")]
  public class AuthController : Controller
  {
    private readonly IAuthRepository _repo;
    public AuthController(IAuthRepository repo)
    {
      this._repo = repo;
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register(string username, string password)
    {
        // validate request


        username = username.ToLower();

        if (await _repo.UserExists(username))
            return BadRequest("Username is already taken");

        var userToCreate = new User
        {
            Username = username
        };
        // notice that we don't set the password here because it'll be set
        // with the repository register class

        var createUser = await _repo.Register(userToCreate, password);



        return StatusCode(201);
    }

  }
}
```

Notice that we are adding the repository as a **dependency injection** in the **constructor class**

---

## Data Transfer Objects (DTOs)

These are typically use when we want to pass object from one type of system to another type of system

So we will create a new folder called **Dtos** and inside it a new class called UserForRegisterDto, notice the convention when creating dtos classes

```csharp
    public class UserForRegisterDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
```

Then we pass the **dto class** as an argument of the controller's Register method 

```csharp
    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] UserForRegisterDto userForRegisterDto) // here we pass the dto
    {
        // implementation ...
    }
```

Notice that we are using the `[FromBody]` hint so that the API knows where to look for the information

---

## Validating in the API

So far our API it's looking good but we need to implement validation because if we tried to insert a blank username or password the api will work without any complain

So we are going to implement data anotations inside the Dto class

```csharp
    public class UserForRegisterDto
    {   
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 4, ErrorMessage = "You must specify a password between 4 and 8 characters")]
        public string Password { get; set; }
    }
```

Then we modify the controller's Register method to add the validation, at the end the method will look like this

```csharp
   [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody]UserForRegisterDto userForRegisterDto)
    {
        userForRegisterDto.Username = userForRegisterDto.Username.ToLower();

        if (await _repo.UserExists(userForRegisterDto.Username))
            ModelState.AddModelError("Username", "Username already exists");

        // validate request
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        var userToCreate = new User
        {
            Username = userForRegisterDto.Username
        };
        // notice that we don't set the password here because it'll be set
        // with the repository register class

        var createUser = await _repo.Register(userToCreate, userForRegisterDto.Password);
        return StatusCode(201);
    }
```

Notice that we creating a model state when the username alredy exist so now we when raise that error the respond will be in json format an not in plain text as it used to be

---

## Token Authentication

First we create a new Dto called `UserForLoginDto.cs` we'll use this as a parameter for the login method

```csharp
      public class UserForLoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
```


Now we need to implete the **jwt** so we don't have to go to the database every time the user makes a request, so in order to implement  **jwtokens** we add a new method to the `AuthController` class 

```csharp
 [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody]UserForLoginDto userForLoginDto)
    {
      var userFromRepo = await _repo.Login(userForLoginDto.Username.ToLower(), userForLoginDto.Password);

      if (userFromRepo == null)
        return Unauthorized();

      // this chunk will generate the token
      var key = Encoding.ASCII.GetBytes("super secret key");
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(new Claim[]
          {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),// this will set the userid
                new Claim(ClaimTypes.Name, userFromRepo.Username)// here the username
          }),

        Expires = DateTime.Now.AddDays(1),
        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)// here you pass the secret key which is th last part of the token (Signature)
      };

      var tokenHandler = new JwtSecurityTokenHandler();
      var token = tokenHandler.CreateToken(tokenDescriptor);

      var tokenString = tokenHandler.WriteToken(token);
      return Ok( new {tokenString});

    }
```

The last video is still missing

all the security with JWT is in the first commit of caja.api util the video



