

# Using Mysql

## Installing Pomelo Vendor

Open the command propmt of vscode with `command-shift-P` and in there select
**install nuget package** in there paste **Pomelo.EntityFrameworkCore.MySql**
and select a stable version

## Adding Configuration

Inside the `appsettings.json` sile add the following configuration

```json

  "ConnectionStrings": {

    "DefaultConnection": "Server=localhost; Database=mauwi; Uid=appuser; Pwd=password"

  }

```

and also in the `Starup.cs` file inside the `ConfigureServices` method add the
following configuration

```csharp
  public void ConfigureServices(IServiceCollection services)
  {
      services.AddDbContext<DataContext>(x => x.UseMySql(Configuration.GetConnectionString("DefaultConnection")));
  }
```

Then don't forget to add the migration and update the database










